// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: stan/protocol.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const StartPosition$json = const {
  '1': 'StartPosition',
  '2': const [
    const {'1': 'NewOnly', '2': 0},
    const {'1': 'LastReceived', '2': 1},
    const {'1': 'TimeDeltaStart', '2': 2},
    const {'1': 'SequenceStart', '2': 3},
    const {'1': 'First', '2': 4},
  ],
};

const PubMsg$json = const {
  '1': 'PubMsg',
  '2': const [
    const {'1': 'clientID', '3': 1, '4': 1, '5': 9, '10': 'clientID'},
    const {'1': 'guid', '3': 2, '4': 1, '5': 9, '10': 'guid'},
    const {'1': 'subject', '3': 3, '4': 1, '5': 9, '10': 'subject'},
    const {'1': 'reply', '3': 4, '4': 1, '5': 9, '10': 'reply'},
    const {'1': 'data', '3': 5, '4': 1, '5': 12, '10': 'data'},
    const {'1': 'connID', '3': 6, '4': 1, '5': 12, '10': 'connID'},
    const {'1': 'sha256', '3': 10, '4': 1, '5': 12, '10': 'sha256'},
  ],
};

const PubAck$json = const {
  '1': 'PubAck',
  '2': const [
    const {'1': 'guid', '3': 1, '4': 1, '5': 9, '10': 'guid'},
    const {'1': 'error', '3': 2, '4': 1, '5': 9, '10': 'error'},
  ],
};

const MsgProto$json = const {
  '1': 'MsgProto',
  '2': const [
    const {'1': 'sequence', '3': 1, '4': 1, '5': 4, '10': 'sequence'},
    const {'1': 'subject', '3': 2, '4': 1, '5': 9, '10': 'subject'},
    const {'1': 'reply', '3': 3, '4': 1, '5': 9, '10': 'reply'},
    const {'1': 'data', '3': 4, '4': 1, '5': 12, '10': 'data'},
    const {'1': 'timestamp', '3': 5, '4': 1, '5': 3, '10': 'timestamp'},
    const {'1': 'redelivered', '3': 6, '4': 1, '5': 8, '10': 'redelivered'},
    const {
      '1': 'redeliveryCount',
      '3': 7,
      '4': 1,
      '5': 13,
      '10': 'redeliveryCount'
    },
    const {'1': 'CRC32', '3': 10, '4': 1, '5': 13, '10': 'CRC32'},
  ],
};

const Ack$json = const {
  '1': 'Ack',
  '2': const [
    const {'1': 'subject', '3': 1, '4': 1, '5': 9, '10': 'subject'},
    const {'1': 'sequence', '3': 2, '4': 1, '5': 4, '10': 'sequence'},
  ],
};

const ConnectRequest$json = const {
  '1': 'ConnectRequest',
  '2': const [
    const {'1': 'clientID', '3': 1, '4': 1, '5': 9, '10': 'clientID'},
    const {
      '1': 'heartbeatInbox',
      '3': 2,
      '4': 1,
      '5': 9,
      '10': 'heartbeatInbox'
    },
    const {'1': 'protocol', '3': 3, '4': 1, '5': 5, '10': 'protocol'},
    const {'1': 'connID', '3': 4, '4': 1, '5': 12, '10': 'connID'},
    const {'1': 'pingInterval', '3': 5, '4': 1, '5': 5, '10': 'pingInterval'},
    const {'1': 'pingMaxOut', '3': 6, '4': 1, '5': 5, '10': 'pingMaxOut'},
  ],
};

const ConnectResponse$json = const {
  '1': 'ConnectResponse',
  '2': const [
    const {'1': 'pubPrefix', '3': 1, '4': 1, '5': 9, '10': 'pubPrefix'},
    const {'1': 'subRequests', '3': 2, '4': 1, '5': 9, '10': 'subRequests'},
    const {'1': 'unsubRequests', '3': 3, '4': 1, '5': 9, '10': 'unsubRequests'},
    const {'1': 'closeRequests', '3': 4, '4': 1, '5': 9, '10': 'closeRequests'},
    const {'1': 'error', '3': 5, '4': 1, '5': 9, '10': 'error'},
    const {
      '1': 'subCloseRequests',
      '3': 6,
      '4': 1,
      '5': 9,
      '10': 'subCloseRequests'
    },
    const {'1': 'pingRequests', '3': 7, '4': 1, '5': 9, '10': 'pingRequests'},
    const {'1': 'pingInterval', '3': 8, '4': 1, '5': 5, '10': 'pingInterval'},
    const {'1': 'pingMaxOut', '3': 9, '4': 1, '5': 5, '10': 'pingMaxOut'},
    const {'1': 'protocol', '3': 10, '4': 1, '5': 5, '10': 'protocol'},
    const {'1': 'publicKey', '3': 100, '4': 1, '5': 9, '10': 'publicKey'},
  ],
};

const Ping$json = const {
  '1': 'Ping',
  '2': const [
    const {'1': 'connID', '3': 1, '4': 1, '5': 12, '10': 'connID'},
  ],
};

const PingResponse$json = const {
  '1': 'PingResponse',
  '2': const [
    const {'1': 'error', '3': 1, '4': 1, '5': 9, '10': 'error'},
  ],
};

const SubscriptionRequest$json = const {
  '1': 'SubscriptionRequest',
  '2': const [
    const {'1': 'clientID', '3': 1, '4': 1, '5': 9, '10': 'clientID'},
    const {'1': 'subject', '3': 2, '4': 1, '5': 9, '10': 'subject'},
    const {'1': 'qGroup', '3': 3, '4': 1, '5': 9, '10': 'qGroup'},
    const {'1': 'inbox', '3': 4, '4': 1, '5': 9, '10': 'inbox'},
    const {'1': 'maxInFlight', '3': 5, '4': 1, '5': 5, '10': 'maxInFlight'},
    const {'1': 'ackWaitInSecs', '3': 6, '4': 1, '5': 5, '10': 'ackWaitInSecs'},
    const {'1': 'durableName', '3': 7, '4': 1, '5': 9, '10': 'durableName'},
    const {
      '1': 'startPosition',
      '3': 10,
      '4': 1,
      '5': 14,
      '6': '.pb.StartPosition',
      '10': 'startPosition'
    },
    const {
      '1': 'startSequence',
      '3': 11,
      '4': 1,
      '5': 4,
      '10': 'startSequence'
    },
    const {
      '1': 'startTimeDelta',
      '3': 12,
      '4': 1,
      '5': 3,
      '10': 'startTimeDelta'
    },
  ],
};

const SubscriptionResponse$json = const {
  '1': 'SubscriptionResponse',
  '2': const [
    const {'1': 'ackInbox', '3': 2, '4': 1, '5': 9, '10': 'ackInbox'},
    const {'1': 'error', '3': 3, '4': 1, '5': 9, '10': 'error'},
  ],
};

const UnsubscribeRequest$json = const {
  '1': 'UnsubscribeRequest',
  '2': const [
    const {'1': 'clientID', '3': 1, '4': 1, '5': 9, '10': 'clientID'},
    const {'1': 'subject', '3': 2, '4': 1, '5': 9, '10': 'subject'},
    const {'1': 'inbox', '3': 3, '4': 1, '5': 9, '10': 'inbox'},
    const {'1': 'durableName', '3': 4, '4': 1, '5': 9, '10': 'durableName'},
  ],
};

const CloseRequest$json = const {
  '1': 'CloseRequest',
  '2': const [
    const {'1': 'clientID', '3': 1, '4': 1, '5': 9, '10': 'clientID'},
  ],
};

const CloseResponse$json = const {
  '1': 'CloseResponse',
  '2': const [
    const {'1': 'error', '3': 1, '4': 1, '5': 9, '10': 'error'},
  ],
};
