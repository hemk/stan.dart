part of stan_dart;

class ProtocolHandler {
  static const String   DEFAULT_PREFIX                = '_STAN.discover';
  static const String   DEFAULT_CLUSTER_ID            = 'stan';
  static const int      DEFAULT_SUBACK_SECONDS        = 3;
  static const int      DEFAULT_MAX_INFLIGHT          = 1;
  static const Duration DEFAULT_PING_INTERVAL         = nats.ClientOptions.DEFAULT_PING_INTERVAL;
  static const int      DEFAULT_MAX_PINGS_OUTSTANDING = nats.ClientOptions.DEFAULT_MAX_PINGS_OUTSTANDING;

  final String      connectionId = Uuid().v4();
  List<int>    get _connectionId => ascii.encode(this.connectionId);

  final String discoveryPrefix;
  final String clusterId;
  final String clientId;

  ConnectResponse _clientConfig;
  Heartbeat       _heartbeat;

  final nats.Client _natsClient;

  ProtocolHandler(this._natsClient, this.clientId, {String clusterId, String discoveryPrefix}):
      this.clusterId       = clusterId??DEFAULT_CLUSTER_ID,   // apply default even if NULL is passed
      this.discoveryPrefix = discoveryPrefix??DEFAULT_PREFIX // apply default even if NULL is passed
  {
    this._heartbeat = Heartbeat(DEFAULT_PING_INTERVAL, this._sendPing, maxMissed: DEFAULT_MAX_PINGS_OUTSTANDING)..cancel(); // -- wait to start until we connect
  }

  Future<void> connect() async {
    ConnectRequest request = ConnectRequest()
      ..clientID       = this.clientId
      ..heartbeatInbox = this.connectionId
      ..connID         = this._connectionId
      ..protocol       = 1
      ..pingInterval   = this._natsClient.options.pingInterval.inSeconds
      ..pingMaxOut     = this._natsClient.options.maxPingsOutstanding;

    ConnectResponse response = await _ask('${this.discoveryPrefix}.${this.clusterId}', request, encoding: null);

    var completer = Completer();
    if (response.hasError()) { // -- failed
      completer.completeError(response.error);

    } else { // -- success
      this._clientConfig = response;
      _onConnect();
      completer.complete();
    }

    return completer.future;
  }

  Future<void> publish(String subject, dynamic payload, {Encoding encoding = ascii}) async {
    PubMsg request = PubMsg()
      ..clientID = this.clientId
      ..guid     = Uuid().v4()
      ..subject  = subject
      ..data     = (encoding == null || payload is List<int> ? payload : encoding.encode(payload))
      ..connID   = this._connectionId;

    await _ask('${this._clientConfig.pubPrefix}.${subject}', request, encoding: encoding);
  }

  /// Subscribe to a given subject. Messages are passed to the provided callback.
  /// [maxInFlight] Maximum inflight messages without an acknowledgement allowed
  /// [ackWaitSeconds] Timeout for receiving an acknowledgement from the client
  /// [durableName] Optional durable name which survives client restarts
  /// [startPosition] An enumerated type specifying the point in history to start replaying data
  /// [startSequence] Optional start sequence number
  /// [startTimeDelta] Optional start time
  Future<Subscription> subscribe(String subject, {
      String         queue,
      int            maxInFlight    = ProtocolHandler.DEFAULT_MAX_INFLIGHT,
      int            ackWaitSeconds = ProtocolHandler.DEFAULT_SUBACK_SECONDS,
      String         durableName,
      StartPosition  startPosition,
      Int64          startSequence,
      Int64          startTimeDelta
  }) async {
    // ======== Listen On Inbox (before subscribing)
    var listenOn = "${subject}_${queue}_${Uuid().v4()}";
    var natsSubscription = _natsClient.subscribe(listenOn, queue: queue); // -- this is where new messages will arrive

    // ======== Create Subscription
    var request = SubscriptionRequest()
      ..clientID = this.clientId
      ..subject  = subject
      ..inbox    = listenOn;

    if (queue          != null) request.qGroup         = queue;
    if (maxInFlight    != null) request.maxInFlight    = maxInFlight;
    if (ackWaitSeconds != null) request.ackWaitInSecs  = ackWaitSeconds;
    if (durableName    != null) request.durableName    = durableName;
    if (startPosition  != null) request.startPosition  = startPosition;
    if (startSequence  != null) request.startSequence  = startSequence;
    if (startTimeDelta != null) request.startTimeDelta = startTimeDelta;

    SubscriptionResponse response = await _ask(_clientConfig.subRequests, request);
    if (response.hasError()) {
      throw Exception(response.error);
    }

    Subscription stanSubscription;
    if (durableName == null) {
      stanSubscription = _subscriptionFactory.subscription(natsSubscription, subject, response.ackInbox, this);
    } else {
      stanSubscription = _subscriptionFactory.durableSubscription(natsSubscription, subject, durableName, response.ackInbox, this);
    }

    return stanSubscription;
  }

  void unsubscribe(Subscription subscription) {
    var tellMessage = UnsubscribeRequest()
      ..clientID = this.clientId
      ..subject  = subscription.subject
      ..inbox    = subscription.inbox;

    if (subscription is DurableSubscription) tellMessage.durableName = subscription.durableName;

    _tell(_clientConfig.unsubRequests, tellMessage);
  }

    // AKA: pause durable subscription
  void closeSubscription(DurableSubscription subscription) {
    var tellMessage = UnsubscribeRequest()
      ..clientID    = this.clientId
      ..subject     = subscription.subject
      ..inbox       = subscription.inbox
      ..durableName = subscription.durableName;

    _tell(_clientConfig.closeRequests, tellMessage);
  }

  void acknowledge(Subscription subscription, DataMessage message) {
    var tellMessage = Ack()
      ..subject  = message.subject
      ..sequence = message.sequence;

    _tell(subscription._ackInbox, tellMessage);
  }

  Future<bool> sendPing({void Function(String error) onError}) async {
    var request = Ping()
      ..connID = this._connectionId;

    PingResponse response = await _ask(_clientConfig.pingRequests, request);

    if (!response.hasError()) {
      return true;
    } else {
      onError(response.error);
      return false;
    }
  }

  //close() {
  //  _natsClient.close();
  //  _onClose();
  //}

  // --

  /// sends a message and waits for a response
  Future<proto.GeneratedMessage> _ask(String subject, proto.GeneratedMessage request, {Encoding encoding = ascii}) async {
    // ======== Send Request
    _logRequest(request, encoding: encoding);
    var responseProto = await _natsClient.request(subject, payload: request.writeToBuffer(), encoding: null);

    // ======== Receive Reply
    proto.GeneratedMessage response;
    switch (request.runtimeType) {
      case ConnectRequest     : response = ConnectResponse.fromBuffer(responseProto.encodedPayload);      break;
      case PubMsg             : response = PubAck.fromBuffer(responseProto.encodedPayload);               break;
      case SubscriptionRequest: response = SubscriptionResponse.fromBuffer(responseProto.encodedPayload); break;
      case Ping               : response = PingResponse.fromBuffer(responseProto.encodedPayload);         break;
    }
    _logResponse(response, encoding: encoding);

    // ======== Return
    return response;
  }

  /// sends a message without waiting for a response
  Future<void> _tell(String subject, proto.GeneratedMessage message, {Encoding encoding = ascii}) async {
    // ======== Send Message
    _logRequest(message, encoding: encoding);
    _natsClient.publish(subject, payload: message.writeToBuffer(), encoding: null);
  }

  void _sendPing() {
    sendPing();
    log.general.finest('Outstanding pings: ${_heartbeat.outstandingCount}/${_heartbeat.maxMissed}');
  }

  void _onConnect() {
    var pingInterval = Duration(seconds: _clientConfig.pingInterval.toInt());
    var maxMissed    = _clientConfig.pingMaxOut;

    _heartbeat
      ..interval  = pingInterval
      ..maxMissed = maxMissed
      ..reset(); // -- start watchdog
  }

  void _onClose() {
    _heartbeat.cancel();
  }

  // --

  void _logRequest(proto.GeneratedMessage request, {Encoding encoding = ascii}) {
    var prefix = _logPrefix(request);
    log.general.info('>>> $prefix: ' + json.encode(ProtobufUtils.messageToMap(request, encoding: encoding)));
  }

  void _logResponse(proto.GeneratedMessage response, {Encoding encoding = ascii}) {
    var prefix = _logPrefix(response);
    log.general.info('<<< $prefix: ' + json.encode(ProtobufUtils.messageToMap(response, encoding: encoding)));
  }

  String _logPrefix(proto.GeneratedMessage message) {
    var prefix = 'STREAMING ';
    switch(message.runtimeType) {
      case ConnectRequest      : prefix += 'CONNECT';     break;
      case ConnectResponse     : prefix += 'CONNECT';     break;
      case PubMsg              : prefix += 'PUBLISH';     break;
      case PubAck              : prefix += 'PUBLISH';     break;
      case SubscriptionRequest : prefix += 'SUBSCRIBE';   break;
      case SubscriptionResponse: prefix += 'SUBSCRIBE';   break;
      case UnsubscribeRequest  : prefix += 'UNSUBSCRIBE'; break;
      case Ack                 : prefix += 'ACK';         break;
      case Ping                : prefix += 'PING';        break;
      case PingResponse        : prefix += 'PONG';        break;
      default                  : prefix += message.runtimeType.toString(); debugger(); break;
    }

    return prefix;
  }
}
