part of stan_dart;

class DurableSubscription extends Subscription {
  final String durableName;

  DurableSubscription._(nats.Subscription delegate, String subject, this.durableName, String ackInbox, ProtocolHandler protocolHandler): super._(delegate, subject, ackInbox, protocolHandler);

  Future<void> pause() => close();

  Future<void> close() async {
    await _closeListeners();

    await _delegate.unsubscribe();
    _protocolHandler.closeSubscription(this);
  }
}
