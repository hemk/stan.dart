part of stan_dart;

class DataMessage {
  final MsgProto  _proto;
  final Uint8List encodedPayload;
  Encoding        encoding;

  DataMessage.fromProto(this._proto, {this.encoding}): encodedPayload = _proto.data;

  String get subject      => _proto.subject;
  Int64  get sequence     => _proto.sequence;
  Int64  get timestamp    => _proto.timestamp;
  bool   get isRedelivery => _proto.redelivered;

  String     _decodedPayload;
  String get  decodedPayload {
    if (_decodedPayload == null) { // -- never decoded, yet
      if (encoding != null) { // -- we know our codec
        _decodedPayload = encoding.decode(encodedPayload);

      } else {  // -- we don't know which codec was used
        nats.log.general.fine("${this.runtimeType}.decodedPayload getter: Payload was supplied already-encoded and without a codec. We don't know how to decode this payload. If you want the raw payload as-supplied, use `${this.runtimeType}.encodedPayload` instead.${this is DataMessage ? ' (Message sequenceId: ${this.sequence})' : ''}");
        _decodedPayload = 'Pre-encoded: ${encodedPayload.length} bytes';
      }
    }

    return _decodedPayload;
  }
}

