part of stan_dart;

class Subscription {
  final nats.Subscription        _delegate;
  final String                   _ackInbox;
  final List<StreamSubscription> _listeners = [];
  final ProtocolHandler          _protocolHandler;
  final String                   subject;

  Stream<DataMessage>     _stream;
  Stream<DataMessage> get  stream => _stream ??= _delegate.stream.transform(_protoTransformer).asBroadcastStream();

  bool _isClosing = false;

  String get inbox => _delegate.subject;
  String get queue => _delegate.queue;

  Subscription._(this._delegate, this.subject, this._ackInbox, this._protocolHandler);

  StreamSubscription<DataMessage> listen(void Function(DataMessage event) onData, {Function onError, void Function() onDone, bool cancelOnError}) {
    var listener = stream.listen(onData, onError: onError, onDone: onDone, cancelOnError: cancelOnError);
    _listeners.add(listener);

    return listener;
  }

  void acknowledge(DataMessage message) {
    _protocolHandler.acknowledge(this, message);
  }

  Future<void> unsubscribe() async {
    await _closeListeners();

    await _delegate.unsubscribe();
    _protocolHandler.unsubscribe(this);
  }

  // --

  final StreamTransformer<nats.DataMessage, DataMessage> _protoTransformer = StreamTransformer.fromHandlers(handleData: (nats.DataMessage source, EventSink<DataMessage> sink) {
    var messageProto = MsgProto.fromBuffer(source.encodedPayload);
    var stanMessage  = DataMessage.fromProto(messageProto);

    sink.add(stanMessage);
  });

  Future<void> _closeListeners() async {
    List<Future> cleanupPromises = [];
    if (!_isClosing) { // -- needs close
      _isClosing = true;
      cleanupPromises = [
        ..._listeners.map((StreamSubscription listener) => listener.cancel().then((_) => _listeners.remove(listener))),
      ];
      await Future.wait(cleanupPromises);
    }
  }
}
