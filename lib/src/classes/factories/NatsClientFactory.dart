part of stan_dart;

class NatsClientFactory {
  nats.Client create(nats.ProtocolHandler handler, [nats.ClientOptions clientOptions]) => nats.Client(handler, clientOptions);

  Future<nats.Client> connect(nats.ServerSet servers, {nats.ServerOptions serverOptions, nats.ClientOptions clientOptions, String username, String password}) => nats.Client.connect(servers, serverOptions: serverOptions, clientOptions: clientOptions, username: username, password: password);

  Future<nats.Client> connectSingle(String host, {int port, nats.ServerOptions serverOptions, nats.ClientOptions clientOptions, String username, String password}) => nats.Client.connectSingle(host, port:port, serverOptions: serverOptions, clientOptions: clientOptions, username: username, password: password);
}

