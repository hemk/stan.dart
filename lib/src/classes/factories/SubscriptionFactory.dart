part of stan_dart;

class SubscriptionFactory {
  Subscription subscription(nats.Subscription delegate, String subject, String ackInbox, ProtocolHandler protocolHandler) => Subscription._(delegate, subject, ackInbox, protocolHandler);

  DurableSubscription durableSubscription(nats.Subscription delegate, String subject, String durableName, String ackInbox, ProtocolHandler protocolHandler) => DurableSubscription._(delegate, subject, durableName, ackInbox, protocolHandler);
}
