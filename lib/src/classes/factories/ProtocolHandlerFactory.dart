part of stan_dart;

class ProtocolHandlerFactory {
  ProtocolHandler create(nats.Client natsClient, String clientId, {String clusterId, String discoveryPrefix}) => ProtocolHandler(natsClient, clientId, clusterId: clusterId, discoveryPrefix: discoveryPrefix);
}
