library stan_dart;

import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';
import 'package:fixnum/fixnum.dart';
import 'package:kiwi/kiwi.dart';
import 'package:logging/logging.dart';
import 'package:nats_dart/nats_dart.dart' as nats;
import 'package:stan_dart/src/utilities/ProtobufUtils.dart';
import 'package:nats_dart/watchdog.dart';
import 'package:protobuf/protobuf.dart' as proto;
import 'package:quiver/strings.dart';

import 'package:uuid/uuid.dart';
import 'generated/stan/protocol.pb.dart';

// ======== Exports
export 'package:nats_dart/nats_dart.dart' show NatsError;

// ======== Parts
part 'src/classes/Client.dart';
part 'src/classes/factories/NatsClientFactory.dart';
part 'src/classes/ProtocolHandler.dart';
part 'src/classes/factories/ProtocolHandlerFactory.dart';
part 'src/classes/Subscription.dart';
part 'src/classes/DurableSubscription.dart';
part 'src/classes/factories/SubscriptionFactory.dart';
part 'src/classes/DataMessage.dart';



// ======== Declarations
typedef MessageHandler = void Function(DataMessage message, {nats.NatsError error});

abstract class log {
  static Logger get general  => Logger('stan.general' );
  static Logger get outgoing => Logger('stan.outgoing');
  static Logger get incoming => Logger('stan.incoming');
}

KiwiContainer container = KiwiContainer()
  ..registerInstance(SubscriptionFactory())
  ..registerInstance(ProtocolHandlerFactory())
  ..registerInstance(NatsClientFactory());

SubscriptionFactory    get _subscriptionFactory    => container.resolve<SubscriptionFactory>();
ProtocolHandlerFactory get _protocolHandlerFactory => container.resolve<ProtocolHandlerFactory>();
NatsClientFactory      get _natsClientFactory      => container.resolve<NatsClientFactory>();
