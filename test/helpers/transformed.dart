import 'package:matcher/matcher.dart';
import 'package:matcher/src/feature_matcher.dart';

typedef _MatchTransformFunction<S, T> = T Function(S value);

Matcher transformed<S, T>(_MatchTransformFunction<S, T> transformer, Matcher matcher) => _TransformFirst(transformer, matcher);

class _TransformFirst<S, T> extends FeatureMatcher<S> {
  final _MatchTransformFunction<S, T> _transformer;
  final Matcher                       _matcher;
  final Map<S, T>                     _transformations = {};

  _TransformFirst(this._transformer, this._matcher);

  @override
  bool typedMatches(S item, Map matchState) {
    return _matcher.matches(_transform(item), matchState);
  }

  @override
  Description describeMismatch(dynamic item, Description mismatchDescription, Map matchState, bool verbose) {
    return _matcher.describeMismatch(_transform(item), mismatchDescription, matchState, verbose);
  }

  T _transform(S item) {
    return _transformations.putIfAbsent(item, () => _transformer(item));
  }
}
