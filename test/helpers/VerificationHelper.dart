import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class VerificationHelper {
  final Map<String, int> capturedArgNameMap;

  VerificationHelper(this.capturedArgNameMap);

  dynamic capturedArgByName(VerificationResult verification, String name) {
    return verification.captured[capturedArgNameMap[name]];
  }

  Map<String, dynamic> capturedArgsToMap(VerificationResult verification) {
    var reversedArgNameMap = capturedArgNameMap.map((name, index) => MapEntry(index, name));

    Map<String, dynamic> argMap = {};

    var i = 0;
    verification.captured.forEach((value) {
      argMap[reversedArgNameMap[i++]] = value;
    });

    return argMap;
  }

  void verifyCapturedArgs(VerificationResult verification, Map<String, dynamic> expectedArgs, {String reasonPrefix = 'Function/Method '}) {
    var actualArgs = capturedArgsToMap(verification);

    expectedArgs.forEach((argName, expectedValue) {
      Matcher matcher = equals(expectedValue);
      if (expectedValue is Matcher) {
        matcher = expectedValue;
      }
      expect(actualArgs[argName], matcher, reason: '$reasonPrefix called with unexpected value for `$argName`');
    });
  }

  void verifyResult(VerificationResult verification, {int expectedCallCount, Map<String, dynamic> expectedArgs, String reasonPrefix}) {
    if (expectedCallCount != null) {
      expect(verification.callCount, equals(expectedCallCount), reason: '$reasonPrefix called an unexpected number of times');
    }

    if (expectedArgs != null && expectedArgs.isNotEmpty) {
      verifyCapturedArgs(verification, expectedArgs, reasonPrefix: reasonPrefix);
    }
  }
}
