import 'dart:convert';
import 'dart:mirrors';
import 'package:faker_extended/faker_extended.dart';
import 'package:stan_dart/generated/stan/protocol.pb.dart';
import 'package:stan_dart/stan_dart.dart';
import 'package:test/test.dart';

void main() {
  group('can be created', () {
    test('fromProto', () {
      var _payload = faker.lorem.sentences(faker.randomGenerator.int(max: 10)).join(' ');
      var _encoding = faker.randomGenerator.element([ascii, utf8, latin1]);
      Map<String, dynamic> expectedProperties = {
        'subject'       : faker.lorem.word(),
        'sequence'      : faker.random.int64(),
        'timestamp'     : faker.random.int64(),
        'isRedelivery'  : faker.random.boolean(),
        'decodedPayload': _payload,
        'encoding'      : _encoding,
        'encodedPayload': _encoding.encode(_payload),
      };

      // ======== Make Call
      var fakeProto = MsgProto()
        ..subject     = expectedProperties['subject']
        ..sequence    = expectedProperties['sequence']
        ..timestamp   = expectedProperties['timestamp']
        ..redelivered = expectedProperties['isRedelivery']
        ..data        = expectedProperties['encodedPayload'];

      var actual = DataMessage.fromProto(fakeProto, encoding: expectedProperties['encoding']);

      // ======== Verify
      var actualMirror = reflect(actual);
      expectedProperties.forEach((key, expectedValue) {
        var actualValue = actualMirror.getField(MirrorSystem.getSymbol(key, actualMirror.type.owner)).reflectee;
        expect(actualValue, equals(expectedValue), reason: 'Message value for `$key` other than expected');
      });
    });
  });
}
