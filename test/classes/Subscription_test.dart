import 'dart:async';
import 'dart:convert';
import 'dart:mirrors';
import 'package:faker_extended/faker_extended.dart';
import 'package:stan_dart/stan_dart.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import '../mocks/NatsSubscriptionMock.dart';
import '../mocks/ProtocolHandlerMock.dart';

void main() {
  // NOTE: our only Subscription constructor is private, meaning our tests can't access it directly, so we need to use reflection
  //       to make this easier for each test to do, use SubscriptionBuilder.build() with optional arguments

  group('can be created', () {
    test('default constructor', () {
      var _subject = faker.lorem.word();
      Map<String, dynamic> expectedProperties = {
        'subject'  : _subject,
        'queue'    : faker.lorem.word(),
        '_ackInbox': faker.lorem.word(),
        'inbox'    : _subject,
      };
      Subscription actual = SubscriptionBuilder.build(expectedProperties['subject'], expectedProperties['queue'], expectedProperties['_ackInbox']);

      var actualMirror = reflect(actual);
      expectedProperties.forEach((key, expectedValue) {
        var actualValue = actualMirror.getField(MirrorSystem.getSymbol(key, actualMirror.type.owner)).reflectee;
        expect(actualValue, equals(expectedValue), reason: 'Subscription value for `$key` other than expected');
      });
    });
  });

  group('event stream', () {
    Subscription subscription;

    /// simulate an incoming message event and allow runLoops and events to propagate
    Future<void> simulateMessage(String payload, {Encoding encoding = ascii}) {
      NatsSubscriptionMock underlyingSubscription = reflect(subscription).getField(MirrorSystem.getSymbol('_delegate', reflect(subscription).type.owner)).reflectee;

      var _encodedPayload = (encoding == null ? payload : encoding.encode(payload));
      return Future
        .sync(() => underlyingSubscription.fakeIncoming(_encodedPayload)) // -- Future.sync() runs this asynchronously NOW
        .then((_) => Future(() {}));                                      // -- Future() allows runLoops and events to propagate before we finish
    }

    setUp(() {
      subscription = SubscriptionBuilder.build();
    });

    test('gets initialized', () async {
      expect(subscription.stream, isA<Stream<DataMessage>>(), reason: 'Subscription message stream other than expected');
    });

    group('can listen', () {
      void messageValidator(message, dynamic expectedMessage, {Encoding encoding = ascii, String label}) {
        var prefix = ((label??'').isNotEmpty ? 'Handler ($label) : ' : '');

        expect(message, isA<DataMessage>(), reason: '${prefix}Captured message of type other than expected');

        if (encoding != null) {
          (message as DataMessage).encoding = encoding;
          expect((message as DataMessage).decodedPayload, expectedMessage, reason: '${prefix}Captured message payload other than expected');
        } else {
          expect((message as DataMessage).encodedPayload, expectedMessage, reason: '${prefix}Captured message payload other than expected');
        }
      }

      test('receives messages', () async {
        String expectedMessage = faker.lorem.sentence();

        // -------- set up our listeners
        var handler = expectAsync1((message) => messageValidator(message, expectedMessage, encoding: ascii), count: 1, reason: 'Handler heard more/less events than the expected 1');
        subscription.listen(handler);

        // -------- send a message
        await simulateMessage(expectedMessage, encoding: ascii);
      });

      test('multiple listeners', () async {
        String expectedMessage = faker.lorem.sentence();

        // -------- set up our listeners
        subscription.listen(expectAsync1((message) => messageValidator(message, expectedMessage, encoding: ascii, label: '1'), count: 1, reason: 'Handler (1) : heard more/less events than the expected 1'));
        subscription.listen(expectAsync1((message) => messageValidator(message, expectedMessage, encoding: ascii, label: '2'), count: 1, reason: 'Handler (2) : heard more/less events than the expected 1'));

        // -------- send a message
        await simulateMessage( expectedMessage, encoding: ascii);
      });
    });

    //@depends('can listen')
    test('can unsubscribe', () async {
      // -------- set up our listeners
      List<StreamSubscription> allListeners = [
        subscription.listen(expectAsync1((_) => null, count: 0, reason: 'Listener (1) : handler received message after allegedly unsubscribing, but shouldn\'t have')), // -- expect this to be called 0 times!
        subscription.listen(expectAsync1((_) => null, count: 0, reason: 'Listener (2) : handler received message after allegedly unsubscribing, but shouldn\'t have')), // -- expect this to be called 0 times!
      ];

      List<StreamSubscription> trackedListeners = reflect(subscription).getField(MirrorSystem.getSymbol('_listeners', reflectClass(Subscription).owner)).reflectee;
      expect(trackedListeners, hasLength(2), reason: 'Initial number of listeners other than expected');

      // -------- unsubscribe
      await subscription.unsubscribe();

      // -------- send a message (shouldn't be received by any listeners)
      await simulateMessage(faker.lorem.sentence());

      // ======== Verify Unsubscription
        // -------- listeners are actually canceled
        int acceptableStates = (
          2 | /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_CLOSED]
          8 | /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_CANCELED]
          16  /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_WAIT_FOR_CANCEL]
        );
        allListeners.forEach((listener) {
          var currentState = reflect(listener).getField(MirrorSystem.getSymbol('_state', reflectClass(listener.runtimeType).owner)).reflectee; // -- private property, need reflection
          expect((currentState & acceptableStates), isNonZero, reason: 'Listener state other than expected');
        });

        expect(trackedListeners, isEmpty, reason: 'Listeners not removed from collection as expected');

        // -------- protocolHandler was called
        ProtocolHandler protocolHandler = reflect(subscription).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(subscription).type.owner)).reflectee;
        var verification = verify(protocolHandler.unsubscribe(captureAny));
        expect(verification.callCount,      equals(1),            reason: 'ProtocolHandler.unsubscribe() called an unexpected number of times');
        expect(verification.captured.first, equals(subscription), reason: 'ProtocolHandler.unsubscribe() called with an unexpected argument'  );

        // -------- underlying subscription was cancelled
        NatsSubscriptionMock underlyingNatsSubscription = reflect(subscription).getField(MirrorSystem.getSymbol('_delegate', reflect(subscription).type.owner)).reflectee;
        verification = verify(underlyingNatsSubscription.unsubscribe());
        expect(verification.callCount,      equals(1),            reason: 'NatsSubscription.unsubscribe() called an unexpected number of times');
    });

    test('can close', () async {
      subscription = SubscriptionBuilder.buildDurable();

      // -------- set up our listeners
      List<StreamSubscription> allListeners = [
        subscription.listen(expectAsync1((_) => null, count: 0, reason: 'Listener (1) : handler received message after allegedly unsubscribing, but shouldn\'t have')), // -- expect this to be called 0 times!
        subscription.listen(expectAsync1((_) => null, count: 0, reason: 'Listener (2) : handler received message after allegedly unsubscribing, but shouldn\'t have')), // -- expect this to be called 0 times!
      ];

      List<StreamSubscription> trackedListeners = reflect(subscription).getField(MirrorSystem.getSymbol('_listeners', reflectClass(Subscription).owner)).reflectee;
      expect(trackedListeners, hasLength(2), reason: 'Initial number of listeners other than expected');

      // -------- unsubscribe
      await (subscription as DurableSubscription).close();

      // -------- send a message (shouldn't be received by any listeners)
      await simulateMessage(faker.lorem.sentence());

      // ======== Verify Unsubscription
        // -------- listeners are actually canceled
        int acceptableStates = (
          2 | /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_CLOSED]
          8 | /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_CANCELED]
          16  /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_WAIT_FOR_CANCEL]
        );
        allListeners.forEach((listener) {
          var currentState = reflect(listener).getField(MirrorSystem.getSymbol('_state', reflectClass(listener.runtimeType).owner)).reflectee; // -- private property, need reflection
          expect((currentState & acceptableStates), isNonZero, reason: 'Listener state other than expected');
        });

        expect(trackedListeners, isEmpty, reason: 'Listeners not removed from collection as expected');

        // -------- protocolHandler was called
        ProtocolHandler protocolHandler = reflect(subscription).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(subscription).type.owner)).reflectee;
        var verification = verify(protocolHandler.closeSubscription(captureAny));
        expect(verification.callCount,      equals(1),            reason: 'ProtocolHandler.unsubscribe() called an unexpected number of times');
        expect(verification.captured.first, equals(subscription), reason: 'ProtocolHandler.unsubscribe() called with an unexpected argument'  );

        // -------- underlying subscription was cancelled
        NatsSubscriptionMock underlyingNatsSubscription = reflect(subscription).getField(MirrorSystem.getSymbol('_delegate', reflect(subscription).type.owner)).reflectee;
        verification = verify(underlyingNatsSubscription.unsubscribe());
        expect(verification.callCount,      equals(1),            reason: 'NatsSubscription.unsubscribe() called an unexpected number of times');
    });
  });
}

// our only constructor is private, meaning our tests can't access it directly, so we need to use reflectioin
abstract class SubscriptionBuilder {

  static Subscription build([String subject, String queue, String ackInbox, ProtocolHandler protocolHandler]) {
    subject         ??= faker.lorem.word();
    queue           ??= faker.lorem.word();
    ackInbox        ??= faker.lorem.word();
    protocolHandler ??= ProtocolHandlerMock();

    var classMirror                = reflectClass(Subscription);
    var underlyingNatsSubscription = NatsSubscriptionMock(subject, queue: queue);
    var instance                   = classMirror.newInstance(MirrorSystem.getSymbol('_', classMirror.owner), [underlyingNatsSubscription, subject, ackInbox, protocolHandler]).reflectee;

    expect(instance, isA<Subscription>(), reason: 'Subscription instance type other than expected');

    return (instance as Subscription);
  }

  static DurableSubscription buildDurable([String subject, String durableName, String queue, String ackInbox, ProtocolHandler protocolHandler]) {
    subject         ??= faker.lorem.word();
    durableName     ??= faker.lorem.word();
    queue           ??= faker.lorem.word();
    ackInbox        ??= faker.lorem.word();
    protocolHandler ??= ProtocolHandlerMock();

    var classMirror                = reflectClass(DurableSubscription);
    var underlyingNatsSubscription = NatsSubscriptionMock(subject, queue: queue);
    var instance                   = classMirror.newInstance(MirrorSystem.getSymbol('_', classMirror.owner), [underlyingNatsSubscription, subject, durableName, ackInbox, protocolHandler]).reflectee;

    expect(instance, isA<DurableSubscription>(), reason: 'DurableSubscription instance type other than expected');

    return (instance as DurableSubscription);
  }
}
