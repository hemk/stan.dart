import 'dart:convert';
import 'dart:mirrors';
import 'package:stan_dart/generated/stan/protocol.pbenum.dart' as enums;
import 'package:nats_dart/nats_dart.dart' as nats;
import 'package:stan_dart/stan_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import '../helpers/VerificationHelper.dart';
import '../mocks/ProtocolHandlerMock.dart';
import '../mocks/SubscriptionMocks.dart';
import '../mocks/factories/NatsClientFactoryMock.dart';
import '../mocks/factories/ProtocolHandlerFactoryMock.dart';
import '../mocks/factories/SubscriptionFactoryMock.dart';

Client                  client;
InstanceMirror      get _handlerMirror       => reflect(client).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(client).type.owner));
ProtocolHandlerMock get handler              => _handlerMirror.reflectee;
nats.Client         get underlyingNatsClient => _handlerMirror.getField(MirrorSystem.getSymbol('_natsClient', _handlerMirror.type.owner)).reflectee;

void main() {
  container
    ..unregister<SubscriptionFactory   >()..registerInstance<SubscriptionFactory   >(SubscriptionFactoryMock()   )
    ..unregister<ProtocolHandlerFactory>()..registerInstance<ProtocolHandlerFactory>(ProtocolHandlerFactoryMock())
    ..unregister<NatsClientFactory     >()..registerInstance<NatsClientFactory     >(NatsClientFactoryMock()     );

  String expectedClientId;
  String expectedClusterId;
  String expectedDiscoveryPrefix;

  setUp(() async {
    expectedClientId        = faker.lorem.word();
    expectedClusterId       = faker.lorem.word();
    expectedDiscoveryPrefix = faker.lorem.word();

    var natsClientMock = container.resolve<NatsClientFactory>().create(null);
    client             = await Client.connectExisting(natsClientMock, expectedClientId,
      clusterId      : expectedClusterId,
      discoveryPrefix: expectedDiscoveryPrefix
    );
  });

  group('connect with', () {
    test('connect()', () async {
      // ======== Create Client
      var expectedHost = faker.internet.uri(faker.random.element(['http', 'https'])) + ':${faker.random.integer(5000, min: 1000)}';
      Client client    = await Client.connect(nats.ServerSet.from([expectedHost]), expectedClientId,
        clusterId:       expectedClusterId,
        discoveryPrefix: expectedDiscoveryPrefix);

      // ======== Validate
      var handlerMirror = reflect(client).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(client).type.owner));

      // -------- protocol handler
      var reflectedHandler = handlerMirror.reflectee;
      expect(reflectedHandler, isA<ProtocolHandler>(), reason: 'ProtocolHandler type other than expected');

      var verification = verify(reflectedHandler.connect());
      expect(verification.callCount, equals(1), reason: 'ProtocolHandler.connect() called an unexpected number of times');

      expect(reflectedHandler.clientId,        equals(expectedClientId),        reason: 'clientId other than expected'       );
      expect(reflectedHandler.clusterId,       equals(expectedClusterId),       reason: 'clusterId other than expected'      );
      expect(reflectedHandler.discoveryPrefix, equals(expectedDiscoveryPrefix), reason: 'discoveryPrefix other than expected');

      // -------- underlying nats client
      var underlyingClient = handlerMirror.getField(MirrorSystem.getSymbol('_natsClient', handlerMirror.type.owner)).reflectee;
      expect(underlyingClient, isA<nats.Client>(), reason: 'Underlying natsClient type other than expected');
    });

    test('connectSingle()', () async {
      // ======== Create Client
      var expectedHost = faker.internet.uri(faker.random.element(['http', 'https'])) + ':${faker.random.integer(5000, min: 1000)}';
      Client client    = await Client.connectSingle(expectedHost, expectedClientId,
        clusterId:       expectedClusterId,
        discoveryPrefix: expectedDiscoveryPrefix);

      // ======== Validate
      var handlerMirror = reflect(client).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(client).type.owner));

      // -------- protocol handler
      var reflectedHandler = handlerMirror.reflectee;
      expect(reflectedHandler, isA<ProtocolHandler>(), reason: 'ProtocolHandler type other than expected');

      var verification = verify(reflectedHandler.connect());
      expect(verification.callCount, equals(1), reason: 'ProtocolHandler.connect() called an unexpected number of times');

      expect(reflectedHandler.clientId,        equals(expectedClientId),        reason: 'clientId other than expected'       );
      expect(reflectedHandler.clusterId,       equals(expectedClusterId),       reason: 'clusterId other than expected'      );
      expect(reflectedHandler.discoveryPrefix, equals(expectedDiscoveryPrefix), reason: 'discoveryPrefix other than expected');

      // -------- underlying nats client
      var underlyingClient = handlerMirror.getField(MirrorSystem.getSymbol('_natsClient', handlerMirror.type.owner)).reflectee;
      expect(underlyingClient, isA<nats.Client>(), reason: 'Underlying natsClient type other than expected');
    });

    test('connectExisting()', () async {
      // ======== Create Client
      var    fakeNatsClient = container.resolve<NatsClientFactory>().create(null);
      Client client         = await Client.connectExisting(fakeNatsClient, expectedClientId,
        clusterId:       expectedClusterId,
        discoveryPrefix: expectedDiscoveryPrefix);

      // ======== Validate
      var handlerMirror = reflect(client).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(client).type.owner));

      // -------- protocol handler
      var reflectedHandler = handlerMirror.reflectee;
      expect(reflectedHandler, isA<ProtocolHandler>(), reason: 'ProtocolHandler type other than expected');

      var verification = verify(reflectedHandler.connect());
      expect(verification.callCount, equals(1), reason: 'ProtocolHandler.connect() called an unexpected number of times');

      expect(reflectedHandler.clientId,        equals(expectedClientId),        reason: 'clientId other than expected'       );
      expect(reflectedHandler.clusterId,       equals(expectedClusterId),       reason: 'clusterId other than expected'      );
      expect(reflectedHandler.discoveryPrefix, equals(expectedDiscoveryPrefix), reason: 'discoveryPrefix other than expected');

      // -------- underlying nats client
      var underlyingClient = handlerMirror.getField(MirrorSystem.getSymbol('_natsClient', handlerMirror.type.owner)).reflectee;
      expect(underlyingClient, isA<nats.Client>(), reason: 'Underlying natsClient type other than expected');
    });
  });

  group('publish() method', () {
    VerificationHelper   verificationHelper;
    Map<String, dynamic> expectedArgs;
    Map<String, int>     argMap;

    VerificationResult _verifyPublishCall(ProtocolHandler handler) {
      var namedCaptures = Map.from(argMap)
        ..remove('subject')  // -- first positional arg
        ..remove('payload'); // -- second positional arg
      Map<Symbol, void> getNamedCaptures() => namedCaptures.map((key, value) => MapEntry(Symbol(key), captureAnyNamed(key)));

      return verify(reflect(handler).invoke(MirrorSystem.getSymbol('publish', reflect(handler).type.owner), [captureAny, captureAny], getNamedCaptures()));
    }

    setUp(() {
      expectedArgs = {};

      argMap = {
        'subject' : 0,
        'payload' : 1,
        'encoding': 2,
      };

      verificationHelper = VerificationHelper(argMap);
    });

    test('with blank subject', () {
      expect(
        () => client.publish(""),
        throwsA(NatsError.BAD_SUBJECT),
        reason: "Blank subject should throw a Nats.Error.BAD_SUBJECT exception, but did not"
      );
    });

    test('with valid subject', () {
      var expectedCallCount = 1;
      expectedArgs.addAll({
        'subject': faker.lorem.word(),
      });

      // ======== Make Call
      client.publish(expectedArgs['subject']);

      // ======== Verify
      var verification = _verifyPublishCall(handler);
      verificationHelper.verifyResult(verification,
        reasonPrefix     : 'ProtocolHandler.publish()',
        expectedCallCount: expectedCallCount,
        expectedArgs     : expectedArgs
      );
    });

    test('with optional arguments', () {
      var expectedCallCount = 1;
      expectedArgs.addAll({
        'payload' : faker.lorem.word(),
        'encoding': faker.random.element([ascii, utf8, latin1]),
      });

      // ======== Make Call
      client.publish(faker.lorem.word(),
        payload : expectedArgs['payload'],
        encoding: expectedArgs['encoding'],
      );

      // ======== Verify

      var verification = _verifyPublishCall(handler);
      verificationHelper.verifyResult(verification,
        reasonPrefix     : 'ProtocolHandler.publish()',
        expectedCallCount: expectedCallCount,
        expectedArgs     : expectedArgs
      );
    });
  });

  group('subscribe() method', () {
    VerificationHelper   verificationHelper;
    Map<String, dynamic> expectedArgs;
    Map<String, int>     argMap;

    VerificationResult _verifySubscribeCall(ProtocolHandler handler) {
      var namedCaptures = Map.from(argMap)..remove('subject'); // -- first positional arg
      Map<Symbol, void> getNamedCaptures() => namedCaptures.map((key, value) => MapEntry(Symbol(key), captureAnyNamed(key)));

      return verify(reflect(handler).invoke(MirrorSystem.getSymbol('subscribe', reflect(handler).type.owner), [captureAny], getNamedCaptures()));
    }

    setUp(() {
      expectedArgs = {
        'maxInFlight'   : ProtocolHandler.DEFAULT_MAX_INFLIGHT,
        'ackWaitSeconds': ProtocolHandler.DEFAULT_SUBACK_SECONDS,
      };

      argMap = {
        'subject'       : 0,
        'queue'         : 1,
        'maxInFlight'   : 2,
        'ackWaitSeconds': 3,
        'durableName'   : 4,
        'startPosition' : 5,
        'startSequence' : 6,
        'startTimeDelta': 7,
      };

      verificationHelper = VerificationHelper(argMap);
    });

    test('with blank subject', () {
      expect(
        () => client.subscribe(""),
        throwsA(NatsError.BAD_SUBJECT),
        reason: "Null subject should throw a NatsError.BAD_SUBJECT exception, but did not"
      );
    });

    test('with valid subject', () async {
      var expectedCallCount = 1;
      expectedArgs.addAll({
        'subject': faker.lorem.word()
      });

      // ======== Make Call
      await client.subscribe(expectedArgs['subject']);

      // ======== Verify
      var verification = _verifySubscribeCall(handler);
      verificationHelper.verifyResult(verification,
        reasonPrefix     : 'ProtocolHandler.subscribe()',
        expectedCallCount: expectedCallCount,
        expectedArgs     : expectedArgs
      );
    });

    test('with optional arguments', () async {
      var expectedCallCount = 1;
      expectedArgs.addAll({
        'queue'         : faker.lorem.word(),
        'durableName'   : faker.lorem.word(),
        'startPosition' : faker.random.element(enums.StartPosition.values),
        'startSequence' : faker.random.int64(),
        'startTimeDelta': faker.random.int64(),
      });

      // ======== Make Call
      await client.subscribe(faker.lorem.word(),
        queue         : expectedArgs['queue'],
        durableName   : expectedArgs['durableName'],
        startPosition : expectedArgs['startPosition'],
        startSequence : expectedArgs['startSequence'],
        startTimeDelta: expectedArgs['startTimeDelta'],
      );

      // ======== Verify
      var verification = _verifySubscribeCall(handler);
      verificationHelper.verifyResult(verification,
        reasonPrefix     : 'ProtocolHandler.subscribe()',
        expectedCallCount: expectedCallCount,
        expectedArgs     : expectedArgs
      );
    });

    test('with handler callback', () async {
      var expectedCallCount = 1;
      var expectedHandler   = HandlerMock((message, {error}) => null);

      var waitForIt = untilCalled(expectedHandler.call(any, error: anyNamed('error')))
        .timeout(Duration(seconds: 1), onTimeout: () => fail('Handler callback unexpectedly never called'))
        .then((_) => verify(expectedHandler.call(any, error: anyNamed('error'))).called(expectedCallCount));

      // ======== Make Call
      SubscriptionMock subscription = await client.subscribe(faker.lorem.word(), handler: expectedHandler);

      // ======== Simulate Message
      subscription.simulateIncomingDataMessage();

      // ======== Verify
      return waitForIt;
    });
  });

  test('sendPing() method', () {
    client.sendPing();
    verify(handler.sendPing()).called(1);
  });
}

// expectAsync1() returns a wrapper of type `function(List(argument1))` which doesn't match our required signature of `function(argument1)`
// so we do this crap so that, instead of `expectAsync1(..., count: 1)` (which operates on a method), we can use `verify(...).called(1)` (which operates on a class);
abstract class HandlerFake extends Fake {
  void call(DataMessage message, {NatsError error});
}

class HandlerMock extends Mock implements HandlerFake {
  final MessageHandler callback;

  HandlerMock(this.callback) {
    when(call(any, error: anyNamed('error'))).thenAnswer((i) => callback(i.positionalArguments.first, error: i.namedArguments['error']));
  }
}
