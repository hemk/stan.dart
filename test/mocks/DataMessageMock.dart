import 'dart:convert';
import 'package:fixnum/fixnum.dart';
import 'package:stan_dart/stan_dart.dart';
import '_ConstructorValueMock.dart';

class DataMessageMock extends ConstructorValueMock implements DataMessage {
  @override String get decodedPayload => (encodedPayload is! List<int> ? encodedPayload : (encoding??ascii).decode(encodedPayload));

  @override String get subject      => values['_proto'].subject;
  @override Int64  get sequence     => values['_proto'].sequence;
  @override Int64  get timestamp    => values['_proto'].timestamp;
  @override bool   get isRedelivery => values['_proto'].redelivered;

  DataMessageMock(Map<String, dynamic> values): super(values..putIfAbsent('payload', () => ''));
}
