import 'package:faker_extended/faker_extended.dart';
import 'package:mockito/mockito.dart';
import 'package:stan_dart/stan_dart.dart';
import 'NatsSubscriptionMock.dart';
import '_ConstructorValueMock.dart';

class ProtocolHandlerMock extends ConstructorValueMock implements ProtocolHandler {
  final SubscriptionFactory _subscriptionFactory = container.resolve<SubscriptionFactory>();

  ProtocolHandlerMock([Map<String, dynamic> values = const {}]): super(values) {
    //when(unsubscribe(any)).thenReturn(null); // -- we'll be verifying (later) that this method actually gets called

    when(subscribe(any,
      queue         : anyNamed('queue'),
      durableName   : anyNamed('durableName'),
      startPosition : anyNamed('startPosition'),
      startSequence : anyNamed('startSequence'),
      startTimeDelta: anyNamed('startTimeDelta'),
    )).thenAnswer((i) {
      var subject  = i.positionalArguments.first;
      var queue    = i.namedArguments[#queue];
      var ackInbox = faker.lorem.word();

      var underlyingChannel = faker.lorem.word();
      var natsSubscription  = NatsSubscriptionMock(underlyingChannel, queue: queue);

      return Future.value(_subscriptionFactory.subscription(natsSubscription, subject, ackInbox, this));
    });
  }

}
