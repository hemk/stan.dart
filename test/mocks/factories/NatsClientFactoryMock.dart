import 'package:nats_dart/nats_dart.dart' as nats;
import 'package:stan_dart/stan_dart.dart';
import 'package:mockito/mockito.dart';

import '../NatsClientMock.dart';

class NatsClientFactoryMock extends Mock implements NatsClientFactory {
  @override
  NatsClientMock create(nats.ProtocolHandler handler, [nats.ClientOptions clientOptions, Map<String, dynamic> testMeta]) => NatsClientMock({'handler': handler, 'clientOptions': clientOptions??nats.ClientOptions()}, testMeta);

  @override
  Future<NatsClientMock> connect(nats.ServerSet servers, {nats.ServerOptions serverOptions, nats.ClientOptions clientOptions, String username, String password}) => NatsClientMock.connect(servers, serverOptions: serverOptions, clientOptions: clientOptions, username: username, password: password);

  @override
  Future<NatsClientMock> connectSingle(String host, {int port, nats.ServerOptions serverOptions, nats.ClientOptions clientOptions, String username, String password}) => NatsClientMock.connectSingle(host, port:port, serverOptions: serverOptions, clientOptions: clientOptions, username: username, password: password);
}
