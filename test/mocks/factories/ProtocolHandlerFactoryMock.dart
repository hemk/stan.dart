import 'package:nats_dart/nats_dart.dart' as nats;
import 'package:stan_dart/stan_dart.dart';
import 'package:mockito/mockito.dart';

import '../ProtocolHandlerMock.dart';

class ProtocolHandlerFactoryMock extends Mock implements ProtocolHandlerFactory {
  @override
  ProtocolHandlerMock create(nats.Client natsClient, String clientId, {String clusterId, String discoveryPrefix}) => ProtocolHandlerMock({'_natsClient': natsClient, 'clientId': clientId, 'clusterId': clusterId, 'discoveryPrefix': discoveryPrefix});
}
