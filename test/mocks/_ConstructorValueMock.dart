import 'dart:mirrors';
import 'package:mockito/mockito.dart';

abstract class ConstructorValueMock extends Mock {
  final Map<String, dynamic> values;
  ConstructorValueMock(this.values);

  @override
  dynamic noSuchMethod(Invocation invocation) {
    if (invocation.isGetter) {
      return (values??{})[MirrorSystem.getName(invocation.memberName)];
    } else {
      return super.noSuchMethod(invocation);
    }
  }
}
