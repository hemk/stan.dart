import 'dart:io';
import 'package:test_coverage/test_coverage.dart';

int main() {
  var packageRoot = Directory.current;
  var testFiles   = findTestFiles(packageRoot);

  generateMainScript(packageRoot, testFiles);

  return testFiles.length;
}
