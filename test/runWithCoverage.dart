import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:faker_extended/faker_extended.dart';
import 'package:test_coverage/test_coverage.dart';
import 'generateTestRunner.dart' as generator show main;

Future<void> main() async {
  var projectRoot = Directory.current;

  print('discovering test files...');
    var count = generator.main();
    print('\tdiscovered $count test suites');

  print('running tests and collecting coverage...');
    var port = faker.randomGenerator.integer(9999, min: 4999);
    await runTestsAndCollect(projectRoot.path, port.toString());
    print('\ttests complete');

  print('calculating code coverage...');
    var lcov = File(path.join(projectRoot.path, 'coverage', 'lcov.info'));
    var coverage = calculateLineCoverage(lcov);

  print('');
  print('--------------------------');
  print('total coverage: ' + (coverage * 100).toStringAsFixed(2) + '%');

  await lcov.parent.delete(recursive: true);
}
