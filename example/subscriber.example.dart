import 'dart:convert';
import 'dart:io';

import 'package:nats_dart/nats_dart.dart' show log;
import 'package:stan_dart/stan_dart.dart' as stan;
import './helpers/example_helpers.dart';

String stanClusterId = Platform.environment['STAN_CLUSTER_ID']??'default';

void _main() {
  log.general.config('starting up...');

  // ======== Consumer
  withConsumerColorSet((client) async { // -- this wrapper is just to help with colorizing our output
    var streamingClient = await stan.Client.connectExisting(client, 'consumer', clusterId: stanClusterId); // -- connect to stan with our already-connected nats client

    stan.Subscription subscription;
    subscription = await streamingClient.subscribe('myTopic', handler: (stan.DataMessage message, {stan.NatsError error}) {
      message.encoding = ascii;
      log.general.info('Message Received! ::: ${message.decodedPayload}');
      subscription.acknowledge(message);
    });
  });
}

void main() {
  setupLogger();
  _main();
}
