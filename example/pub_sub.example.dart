import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:nats_dart/nats_dart.dart' show log;
import 'package:stan_dart/stan_dart.dart' as stan;
import './helpers/example_helpers.dart';

int    maxMessages   = 0; // -- 0 = infinite
String stanClusterId = Platform.environment['STAN_CLUSTER_ID']??'default';

void _main() {
  log.general.config('starting up...');

  // ======== Consumer
  withConsumerColorSet((consumer) async { // -- this wrapper is just to help with colorizing our output
    var streamingConsumer = await stan.Client.connectExisting(consumer, 'consumer', clusterId: stanClusterId); // -- connect to stan with our already-connected nats client
    print('connected!');

    var subscription = await streamingConsumer.subscribe('myTopic');
    print('subscribed');

    subscription.listen((stan.DataMessage message, {stan.NatsError error}) {
      message.encoding = ascii;
      log.general.warning('<<< STREAMING MSG: ${message.decodedPayload}');
      subscription.acknowledge(message);
    });
  });

  // ======== Producer
  withProducerColorSet((producer) async { // -- this wrapper is just to help with colorizing our output
    var streamingProducer = await stan.Client.connectExisting(producer, 'publisher', clusterId: 'default');
    print('connected!');

    var counter = 0;
    await Future.delayed(Duration(seconds: 0), () => streamingProducer.publish('myTopic', payload: 'this is an event (#${++counter}) on topic: myTopic')); // -- let the client encode it for us (with the default encoder (ascii))
    counter++;

    Timer.periodic(Duration(seconds: 1), (timer) {
      streamingProducer.publish('myTopic', payload: 'this is another event (#${++counter}) on topic: myTopic');
      if ((maxMessages??0) > 0 && counter >= maxMessages) {
        timer.cancel();
      }
    }); // -- encode it ourselves and tell the client not to worry about it
  });
}

void main() {
  setupLogger();
  _main();
}
