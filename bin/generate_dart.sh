#!/bin/bash

set -e # abort all if any command fails
set -u # error if referencing non-existent variables

normalizePath() {
  normalized="$(echo "$1" | sed -e "s/~/\${HOME}/" | envsubst)" # replace ~ in user input with $HOME and resolve the value

  command -v cygpath > /dev/null
  if [ "$?" -eq 0 ] ; then # USE_CYGPATH (git for windows)
    normalized="$(cygpath -w "$normalized")"
  fi

  echo "$normalized"
}


# set our working directory
BASE_PATH="$(cygpath "$(dirname "${BASH_SOURCE[0]}")")"

# fetch our proto files
$("${BASE_PATH}/fetch_protos.sh")

# prep to generate
PROTO_DIR=$(realpath "${BASE_PATH}/../protos")
DEST_DIR=$(realpath "${BASE_PATH}/../lib/generated")
BAK_DIR=$(realpath "${DEST_DIR}.bak.dir")
TEMP_DIR=$(realpath "${DEST_DIR}.tmp.dir")

rm -rf "$BAK_DIR"
rm -rf "$TEMP_DIR"
mkdir -p "$PROTO_DIR"
mkdir -p "$DEST_DIR"
mkdir -p "$TEMP_DIR"



# generate our files
NORMALIZED="$(normalizePath "$TEMP_DIR")"
#set -x # show commands as they are run
find "$PROTO_DIR" -iname '*.proto' | xargs -t protoc --dart_out="grpc:$NORMALIZED" -I "$PROTO_DIR"
#set +x # stop showing commands as they are run

# add linter options (see: https://github.com/dart-lang/protobuf/issues/368 and https://github.com/dart-lang/protobuf/issues/369)
# make compatible with this project's extra-pedantic settings
find "$NORMALIZED" -iname '*.dart' -exec sed -i '1s;^;// added ex post facto by /bin/generate_dart.sh\n// see: https://github.com/dart-lang/protobuf/issues/368\n// see: https://github.com/dart-lang/protobuf/issues/369\n// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering\n\n;' {} \;

# apply dart standard-formatting
dartfmt -w "$NORMALIZED"



# cleanup
mv "$DEST_DIR" "$BAK_DIR" && rm -rf "$DEST_DIR" && mv "$TEMP_DIR" "$DEST_DIR" && rm -rf "$BAK_DIR"
