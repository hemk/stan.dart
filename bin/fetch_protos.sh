#!/bin/bash

set -e # abort all if any command fails
set -u # error if referencing non-existent variables

DEST_DIR="../protos"
BAK_DIR="${DEST_DIR}.bak.dir"
TEMP_DIR="${DEST_DIR}.temp.dir"

VERSION_STAN="master"
VERSION_GOGO="v1.3.1"

# set our working directory
cd "$(cygpath "$(dirname "${BASH_SOURCE[0]}")")"

rm -rf "$BAK_DIR"
rm -rf "$TEMP_DIR"
mkdir -p "$DEST_DIR"



# -------- this file is maintained by NATS themselves and it declares all the endpoints/functions and datatypes their server exposes
SUB_PATH=""
TARGET="${DEST_DIR}.temp.dir/stan/${SUB_PATH}"
set -x # show commands as they are run
mkdir -p "$TARGET" && curl -L "https://raw.githubusercontent.com/nats-io/stan.go/${VERSION_STAN}/pb/${SUB_PATH}/protocol.proto" > "${TARGET}/protocol.proto"
set +x # stop showing commands as they are run

# -------- this file is maintained by GoGo and is a dependency of the NATS proto file above
SUB_PATH=""
TARGET="${DEST_DIR}.temp.dir/github.com/gogo/protobuf/gogoproto/${SUB_PATH}"
set -x # show commands as they are run
mkdir -p "$TARGET" && curl -L "https://raw.githubusercontent.com/gogo/protobuf/${VERSION_GOGO}/gogoproto/${SUB_PATH}/gogo.proto" > "${TARGET}/gogo.proto"
set +x # stop showing commands as they are run



mv "$DEST_DIR" "$BAK_DIR" && rm -rf "$DEST_DIR" && mv "$TEMP_DIR" "$DEST_DIR" && rm -rf "$BAK_DIR"
